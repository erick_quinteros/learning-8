context('testing loadAnchorData() in ANCHOR module')
print(Sys.time())

# load library and source script
library(properties)
library(Learning)
library(Learning.DataAccessLayer)
source(sprintf("%s/anchor/code/utils.r",homedir))
source(sprintf("%s/anchor/code/loadAnchorData.R",homedir))

# reset mock data
requiredMockDataList <- list() # use module own data
requiredMockDataList_module <- list(pfizerusdev_learning=c('AccountDateLocationScores','RepCalendarAdherence','RepAccountCalendarAdherence'),pfizerusdev=c('Interaction','Facility','Rep','InteractionType','InteractionAccount','RepTeamRep','Account'),pfizerusdev_stage=c('RepAccountAssignment_arc'),pfizerusdev_archive=c('STG_Call2_vod__c_H'))
resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,dbname_cs,requiredMockDataList,requiredMockDataList_module,'anchor')

# get required parameter input from learning.properties file
buildUID <- readModuleConfig(homedir, 'anchor','buildUID')
config <- read.properties(sprintf('%s/anchor/tests/data/%s/learning.properties',homedir,buildUID))
# nightly and manual predictionRunDate are same in learning.properties. will need update if this changes
predictRundate <- getConfigurationValueNew(config,"LE_AN_manualPredictRundate")
predictRundate <- as.Date(predictRundate)
predictAhead <- getConfigurationValueNew(config,"LE_AN_predictAhead") # predict how many days in the future
historyWindow <- getConfigurationValueNew(config,"LE_AN_historyWindow") # how many days in history to look back
startDate <- predictRundate - historyWindow
predictAheadDayList <- utils.generatePredictAheadDayList(predictRundate, predictAhead)

# set up db connnection
dataAccessLayer.common.initializeConnections(dbuser, dbpassword, dbhost, dbname, port)

# test funcs
testResult <- function(dta, calculate_adl, calendarAdherenceOn, calculate_calendarAdherence) {
  expect_length(dta, 10)
  INTERACTIONS_REP_new <- dta[["INTERACTIONS_REP"]]
  future_new <- dta[["future"]]
  repAccountAssignments_new <- dta[["repAccountAssignments"]]
  repTeamRep_new <- dta[["repTeamRep"]]
  INTERACTIONS_ACCOUNT_new <- dta[["INTERACTIONS_ACCOUNT"]]
  accountDateLocationScores_new <- dta[["accountDateLocationScores"]]
  newRepList <- dta[["newRepList"]]
  repCalendarAdherence_new <- dta[["repCalendarAdherence"]]
  repAccountCalendarAdherence_new <- dta[["repAccountCalendarAdherence"]]
  callHistory_new <- dta[["callHistory"]]

  test_that(sprintf("result has correct length/dimension for calculate_adl=%s, calendarAdherenceOn=%s, calculate_calendarAdherence=%s", calculate_adl, calendarAdherenceOn, calculate_calendarAdherence), {
    expect_equal(dim(INTERACTIONS_REP_new),c(4692,6))
    expect_equal(dim(future_new),c(46,7))
    expect_equal(dim(repAccountAssignments_new),c(1012,2))
    expect_equal(dim(repTeamRep_new),c(0,2))
    expect_equal(length(newRepList),0)
    if (calculate_adl) {
      expect_equal(dim(INTERACTIONS_ACCOUNT_new),c(5343,6))
      expect_true(is.null(accountDateLocationScores_new))
    } else {
      expect_equal(dim(accountDateLocationScores_new),c(3661,8))
      expect_true(is.null(INTERACTIONS_ACCOUNT_new))
    }
    if (calendarAdherenceOn) {
      if (calculate_calendarAdherence) {
        expect_equal(dim(callHistory_new), c(25,6))
        expect_true(is.null(repCalendarAdherence_new))
        expect_true(is.null(repAccountCalendarAdherence_new))
      } else {
        expect_true(is.null(callHistory_new))
        expect_equal(dim(repCalendarAdherence_new), c(2,5))
        expect_equal(dim(repAccountCalendarAdherence_new), c(8,6))
      }
    } else {
      expect_true(is.null(callHistory_new))
      expect_true(is.null(repCalendarAdherence_new))
      expect_true(is.null(repAccountCalendarAdherence_new))
    }
  })


  test_that(sprintf("result are the same as saved for calculate_adl=%s, calendarAdherenceOn=%s, calculate_calendarAdherence=%s",calculate_adl, calendarAdherenceOn, calculate_calendarAdherence), {
    load(sprintf('%s/anchor/tests/data/from_loadAnchorData_INTERACTIONS_REP.RData', homedir))
    load(sprintf('%s/anchor/tests/data/from_loadAnchorData_future.RData', homedir))
    load(sprintf('%s/anchor/tests/data/from_loadAnchorData_repAccountAssignments.RData', homedir))
    load(sprintf('%s/anchor/tests/data/from_loadAnchorData_repTeamRep.RData', homedir))
    load(sprintf('%s/anchor/tests/data/from_loadAnchorData_INTERACTIONS_ACCOUNT.RData', homedir))
    load(sprintf('%s/anchor/tests/data/from_loadAnchorData_accountDateLocationScores.RData', homedir))
    load(sprintf('%s/anchor/tests/data/from_loadAnchorData_callHistory.RData', homedir))
    load(sprintf('%s/anchor/tests/data/from_calculateCalendarAdherence_repCalendarAdherence.RData',homedir))
    load(sprintf('%s/anchor/tests/data/from_calculateCalendarAdherence_repAccountCalendarAdherence.RData',homedir))

    expect_equal(INTERACTIONS_REP_new[order(facilityId, repId, date),],INTERACTIONS_REP[order(facilityId, repId, date),])
    expect_equal(future_new[order(repId, date, facilityId, accountId),],future[order(repId, date, facilityId, accountId),])
    expect_equal(future_new[date>=predictRundate & date<=max(predictAheadDayList),],future)
    expect_equal(repAccountAssignments_new, repAccountAssignments)
    expect_equal(repTeamRep_new, repTeamRep)
    expect_equal(newRepList, data.table())
    if (calculate_adl) {
      expect_equal(INTERACTIONS_ACCOUNT_new[order(accountId,facilityId),], INTERACTIONS_ACCOUNT[order(accountId,facilityId),])
    } else {
      accountDateLocationScores$facilityDoWScore <- as.double(accountDateLocationScores$facilityDoWScore)
      accountDateLocationScores$facilityPoDScore <- as.double(accountDateLocationScores$facilityPoDScore)
      expect_equal(accountDateLocationScores_new[order(accountId, facilityId, dayOfWeek, periodOfDay),], accountDateLocationScores[order(accountId, facilityId, dayOfWeek, periodOfDay),])
    }
    if (calendarAdherenceOn) {
      if (calculate_calendarAdherence) {
        expect_equal(callHistory_new, callHistory)
      } else {
        expect_equal(repAccountCalendarAdherence_new[order(repId),names(repAccountCalendarAdherence),with=F], repAccountCalendarAdherence[order(repId),])
        expect_equal(repAccountCalendarAdherence_new[order(repId),names(repAccountCalendarAdherence),with=F], repAccountCalendarAdherence[order(repId, accountId),])
      }
    }
  })
}

# run loadAnchorData for calculate_adl=TRUE & calendarAdherenceOn=FALSE
calculate_adl <- TRUE
calendarAdherenceOn <- FALSE
calculate_calendarAdherence <- FALSE
dta <- loadAnchorData(predictRundate, startDate, predictAheadDayList, calculate_adl, calendarAdherenceOn, calculate_calendarAdherence)
testResult(dta, calculate_adl, calendarAdherenceOn, calculate_calendarAdherence)

# run loadAnchorData for calculate_adl=FALSE & calendarAdherenceOn=FALSE
calculate_adl <- FALSE
calendarAdherenceOn <- FALSE
calculate_calendarAdherence <- FALSE
dta <- loadAnchorData(predictRundate, startDate, predictAheadDayList, calculate_adl, calendarAdherenceOn, calculate_calendarAdherence)
testResult(dta, calculate_adl, calendarAdherenceOn, calculate_calendarAdherence)

# run loadAnchorData for calculate_adl=FALSE & calendarAdherenceOn=TRUE & calculate_calendarAdherence=TRUE
calculate_adl <- FALSE
calendarAdherenceOn <- TRUE
calculate_calendarAdherence <- TRUE
dta <- loadAnchorData(predictRundate, startDate, predictAheadDayList, calculate_adl, calendarAdherenceOn, calculate_calendarAdherence)
testResult(dta, calculate_adl, calendarAdherenceOn, calculate_calendarAdherence)

# run loadAnchorData for calculate_adl=FALSE & calendarAdherenceOn=TRUE & calculate_calendarAdherence=FALSE
calculate_adl <- FALSE
calendarAdherenceOn <- TRUE
calculate_calendarAdherence <- FALSE
dta <- loadAnchorData(predictRundate, startDate, predictAheadDayList, calculate_adl, calendarAdherenceOn, calculate_calendarAdherence)
testResult(dta, calculate_adl, calendarAdherenceOn, calculate_calendarAdherence)

# disconnect DB
dataAccessLayer.common.closeConnections()
