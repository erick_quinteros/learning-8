CREATE TABLE `SimulationMessageSequence`(
  `messageSeqUID` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `messageSeqHash` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `messageID` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `sequenceOrder` int(11) NOT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `learningBuildUID` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `learningRunUID` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  KEY `SimulationMessageSequence_hash_index` (`messageSeqHash`),
  KEY `SimulationMessageSequence_fk_1` (`learningBuildUID`),
  KEY `SimulationMessageSequence_fk_2` (`learningRunUID`)
  /* CONSTRAINT `SimulationMessageSequence_fk_1` FOREIGN KEY (`learningBuildUID`) REFERENCES `LearningBuild` (`learningBuildUID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `SimulationMessageSequence_fk_2` FOREIGN KEY (`learningRunUID`) REFERENCES `LearningRun` (`learningRunUID`) ON DELETE CASCADE ON UPDATE CASCADE */
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
