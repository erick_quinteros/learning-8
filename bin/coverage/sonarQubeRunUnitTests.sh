#!/bin/bash

#goal for this script is to run unit tests to generate .coverage / coverage-*.xml files
#so that sonarqube can run the tests to have accurate coverage %s
#this is called in bitbucket-pipeline.yml to run on a jupyter/all-spark-notebook docker clone
#future tests may require changing the image or creating an additional step for different images

bash bin/runUnitTest.sh -m sparkRepEngagementCalculator -u root -w root_pswd -h 127.0.0.1 -p 3306 -s test
bash bin/runUnitTest.sh -m simulationMessageSequence -u root -w root_pswd -h 127.0.0.1 -p 3306 -s test
